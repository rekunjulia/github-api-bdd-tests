package com.github.api.framework.config;

public enum EnvironmentProperty {
    API_BASE_URI,
    AUTH_BASIC_USERNAME,
    AUTH_BASIC_TOKEN
}
